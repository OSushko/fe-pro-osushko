function Human(name, lastname, age, sex) {
    this.name = name;
    this.lastname = lastname;
    this.age = age;
    this.sex = sex;
};

const humans = [
    new Human('Andrew', 'Smith', 23, 'male'),
    new Human('Alice', 'Gordon', 21, 'female'),
    new Human('Oliver', 'Kahn', 42, 'male'),
    new Human('Emily', 'Russel', 31, 'female')
];

console.log(humans);