//Создайте масив на 50 элементов и заполните каждый элемент его номером.
//Не используя циклы выведите каждый нечетный элемент в параграфе, а четный в диве.

function arr(){
    const arr = new Array(50);
    for (let i = 0; i < arr.length; i++){
        arr[i] = i + 1;
    }
    arr.forEach(show)
}
function show(num){
    if(num % 2 == 0){
        document.write('<div style="color: red;">' + num + '</div>')
    } else if(num % 2 == 1){
        document.write('<p>' + num + '</p>')
    }
}
arr();