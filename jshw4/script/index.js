//Напиши функцию , которая принимает на вход функцию и массив,
// и обрабатывает каждый элемент массива этой функцией, возвращая новый массив.

const myArray = prompt('Хочете квадрати? Задайте кілька чисел через пробіл:').split(' ');
let newArray = [];

function map(fn, array) {
    fn(array);
}

sqr = function (array) {
    for (let i = 0; i < array.length; i++) {
        newArray.push(array[i] ** 2);
    }
    alert(`Ваші квадрати: ${newArray.join(', ')}`);
}

map(sqr, myArray);

//Перепишите функцию, используя оператор '?' или '||'
//Следующая функция возвращает true, если параметр age больше 18.
//В ином случае она задаёт вопрос confirm и возвращает его результат.

// function checkAge (age) {
//     if (age > 18) {
//         return true;
//     } else {
//         return confirm ('Родители разрешили?')
//     }
// }

function checkAge(age) {
    (age > 18) ? alert(true) : confirm('Батьки дозволили?');
}

let userAge = parseInt(prompt('Який Ваш вік?'));

checkAge(userAge);