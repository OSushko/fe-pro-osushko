// Создать объект "Документ", в котором определить свойства "Заголовок, тело, футер, дата". Создать в объекте
// вложенный объект - "Приложение". Создать в объекте "Приложение", вложенные объекты, "Заголовок, тело, футер, дата".
// Создать методы для заполнения и отображения документа.

const myDocument = {
    header: '',
    body: '',
    footer: '',
    date: '',
    application: {
        header: {},
        body: {},
        footer: {},
        date: {},
    },

    fill(){
        this.header = prompt('Введіть назву документа:');
        this.body = prompt('Введіть зміст документа:');
        this.footer = prompt('Введіть висновок документа:');
        this.date = prompt('Введіть дату документа:');
        this.application.header = prompt('Введіть назву додатку:');
        this.application.body = prompt('Введіть зміст додатку:');
        this.application.footer = prompt('Введіть висновок додатку:');
        this.application.date = prompt('Введіть дату додатку:');
    },

    show() {
        document.write(`<p>Назва документу: ${this.header}`);
        document.write(`<p>Зміст документу: ${this.body}`);
        document.write(`<p>Висновок документу: ${this.footer}`);
        document.write(`<p>Дата: ${this.date}`);
        document.write(`<p style="text-indent: 20px">Назва додатку: ${this.application.header}`);
        document.write(`<p style="text-indent: 20px">Зміст додатку: ${this.application.body}`);
        document.write(`<p style="text-indent: 20px">Висновок додатку: ${this.application.footer}`);
        document.write(`<p style="text-indent: 20px">Дата додатку: ${this.application.date}`);
    }
};

myDocument.fill();
myDocument.show();