/*
Використовуючи цикли, намалюйте за допомогою пробілів ( ) та зірочок ($):
- прямокутник;
- прямокутний трикутник;
- рівносторонній трикутник;
- ромб.
*/

document.write('<br/><h1 style="font-size: 20px">Прямокутник</h1><br/>');

let sqHeight = parseInt(prompt('Введіть висоту прямокутника'));
let sqWidth = parseInt(prompt('Введіть ширину прямокутника'));

for (let i = 0; i < sqHeight; i++) {
    for (let j = 0; j < sqWidth; j++) {
        if (j != 0 && j != (sqWidth - 1) && i != 0 && i != (sqHeight - 1)) {
            document.write('$');
        } else {
            document.write('*');
        }
    }
    document.write('<br/>');
}

document.write('<br/><h2 style="font-size: 20px">Прямокутний трикутник</h2><br/>');

for (let i = 0; i < 10; i++) {
    document.write('*');
    for (k = (0 + i); k < 9; k++) {
        if (k != 8 && i != 0) {
            document.write('$');
        } else {
            document.write('*');
        }
    }
    document.write('<br/>');
}

document.write('<br/><h2 style="font-size: 20px">Рівносторонній трикутник</h2><br/>');

for (let i = 0; i < 10; i++) {
    for (let j = (0 + i); j < 10; j++) {
        document.write("&nbsp;")
    }
    document.write("*");
    for (let k = i; k > 0; k--) {
        if (k != 1 && i != 9) {
            document.write("$");
        } else {
            document.write("*");
        }
    }
    document.write("<br/>");
}

document.write('<br/><h2 style="font-size: 20px">Ромб</h2><br/>');

for (let i = 0; i < 10; i++) {
    for (let j = (0 + i); j < 10; j++) {
        document.write("&nbsp;")
    }
    document.write("*");
    for (let k = i; k > 0; k--) {
        if (k != 1) {
            document.write("$");
        } else {
            document.write("*");
        }
    }
    document.write("<br/>");
}

for (let n = 9; n > 0; n--) {
    for (let m = (n - 1); m < 10; m++) {
        document.write("&nbsp;");
    }
    document.write("*");
    for (let p = (n - 1); p > 0; p--) {
        if (p != 1) {
            document.write("$");
        } else {
            document.write("*");
        }
    }
    document.write("<br/>");
}
